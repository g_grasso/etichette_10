# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _description = "Quotation/Sale order"

    sale_order_status = fields.Many2one(
        comodel_name='sale.order.status',
        ondelete='restrict',
        string='Status')
    related_purchase_ids = fields.Many2many(
        comodel_name='purchase.order',
        relation='purchase_sale_rel',
        column1='purchase',
        column2='sale',
        string="Related Purchases",
        )
    related_task_ids = fields.One2many(
        comodel_name='project.task',
        inverse_name='etichette_sale_order_id',
        string="Related tasks"
        )
    delivery_date = fields.Date('Data budget')
    validity_date = fields.Date(readonly=False)
    situation = fields.Char('Situation')
    exclude_total = fields.Boolean('Excl. Total')
    disclaimer = fields.Text(
        'Disclaimer',
        default="I quantitativi di stampa sono soggetti ad una tolleranza del"
        " 10%, potrà quindi variare l'importo della fattura."
        " Eventuali cambiamenti diversi dal preventivo saranno quantificati"
        " separatamente a mezzo consuntivo."
        )

    @api.multi
    def action_confirm(self):
        edits = super(SaleOrder, self).action_confirm()
        for item in self:
            if item.state in ['sale', 'done']:
                for line in item.order_line:
                    if line.product_id.create_task:
                        line.create_task()

        return edits  # super(SaleOrder, self).write(vals)

    @api.multi
    def action_cancel(self):
        edits = super(SaleOrder, self).action_cancel()
        for item in self:
            for task in item.related_task_ids:
                task.unlink()
        return edits  # super(SaleOrder, self).write(vals)

    @api.multi
    @api.onchange('delivery_date')
    def update_line_delivery_date(self):
        for order in self:
            for line in order.order_line:
                line.delivery_date = order.delivery_date


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    delivery_date = fields.Date(string='Data budget')
    line_type = fields.Many2one(
        comodel_name='sale.order.line.type',
        ondelete='restrict',
        string='Type of sale')

    @api.multi
    def create_task(self):
        task = self.env['project.task']
        name = "[{}] ({}) - {} ".format(
            self.order_id.name.encode("utf-8"),
            self.product_uom_qty,
            self.product_id.name.encode("utf-8"),
            )
        project = self.product_id.project_id
        if not project:
            raise Warning(_("Please define a project for products \
                that are to be manages as tasks"))

        if not project.user_id:
            raise Warning(_("Please define a project manager \
                for the product's project"))

        new_task_data = ({
            'name': name,
            'project_id': project.id,
            'partner_id': self.order_id.partner_id.id,
            'date_deadline': self.order_id.delivery_date,
            'user_id': project.user_id.id,
            'plain_description': self.name,
            'etichette_sale_order_id': self.order_id.id,
            'etichette_sale_order_line_id': self.id,
        })
        new_task = task.create(new_task_data)


class SaleOrderStatus(models.Model):
    _name = 'sale.order.status'
    _description = "Quotation/Sale order status"

    name = fields.Char('Name')
    active = fields.Boolean('Active')


class SaleOrderLineType(models.Model):
    _name = 'sale.order.line.type'
    _description = "Type to be selected in the sale order line"

    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
