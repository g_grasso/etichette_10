# -*- coding: utf-8 -*-

from . import account_move
from . import bank
from . import ddt
from . import invoice
from . import partner
from . import payment_term
from . import project_task
from . import product
from . import sale_order
