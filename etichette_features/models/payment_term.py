# -*- coding: utf-8 -*-

from openerp import models, fields, api


class PaymentTerm(models.Model):
    _inherit = "account.payment.term"

    riba_term = fields.Boolean('Riba Payment Term')
