# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    outstanding_debt = fields.Boolean('Outstanding debt')
    client_riba = fields.Boolean(
        related='partner_id.riba_charges',
        string="RIBA charges")
    partner_bank_account_id = fields.Many2one(
        comodel_name='res.partner.bank',
        string='Client bank',
        help="Company bank of the client to be printed in RIBA payment terms")
    related_ddt_ids = fields.Many2many(
        comodel_name='stock.picking.package.preparation',
        relation='ddt_invoice_rel',
        column1='ddt',
        column2='invoice',
        string="Related DDT",
        )
    disclaimer = fields.Text(
        'Disclaimer',
        default="Preghiamo voler controllare i vostri dati indicati in fattura"
        " e comunicare tempestivamente eventuali errori o mancanze. In caso"
        " contrario verranno ritenuti corretti quelli riportati."
        )
    total_discount = fields.Float("Total discount")
    discount_computed = fields.Boolean("Discount computed")
    letter_of_intent = fields.Text('Letter of intent')

    # data bank riba management
    riba_presented = fields.Boolean('Presented RIBA')
    riba_advance_account_id = fields.Many2one(
        comodel_name='account.account',
        string='RIBA account',
        help="Account used to advance the invoice")

    # data bank invoice advance
    is_advanced = fields.Boolean('Advanced')
    percentage_advanced = fields.Float('Advance %')
    advance_deadline = fields.Date('Deadline')
    advanced_amount = fields.Float(
        string='Advanced amount',
        store=True,
        compute='_onchange_compute_percentage_advance')
    advance_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Advance account',
        help="Account used for bank invoice advance")

    @api.onchange('invoice_line_ids', 'discount_computed')
    def _onchange_compute_discount(self):
        if self.invoice_line_ids:
            total = 0
            for line in self.invoice_line_ids:
                if line.price_unit < 0:
                    total += line.price_unit
            self.total_discount = abs(total)
            self.discount_computed = True

    @api.multi
    @api.depends('percentage_advanced')
    def _onchange_compute_percentage_advance(self):
        for item in self:
            if item.is_advanced:
                item.advanced_amount = round(
                    (item.amount_total * item.percentage_advanced / 100), 2)

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res = super(AccountInvoice, self)._onchange_partner_id()

        self.partner_bank_id = self.partner_id.company_bank_id
        self.letter_of_intent = self.partner_id.letter_of_intent

        bank_id = self.env['res.partner.bank'].search([
            ('partner_id', '=', self.partner_id.id)])
        if bank_id:
            self.partner_bank_account_id = bank_id[0]
        return res

    @api.multi
    def add_riba_expenses(self):
        for item in self:
            line = item.env['account.invoice.line']
            inv_product = item.partner_id.riba_service

            if not inv_product.property_account_income_id:
                raise Warning(_("Please define the expense account \
                    of the Riba service product"))
            tax_ids = [(4, p.id) for p in inv_product.taxes_id]

            invoice_line_data = ({
                'invoice_id': item.id,
                'product_id': inv_product.id,
                'name': 'Spese bancarie RIBA',
                'quantity': 1,
                'account_id': inv_product.property_account_income_id.id,
                'price_unit': inv_product.lst_price,
                'invoice_line_tax_ids': tax_ids,
                })
            new_line = line.create(invoice_line_data)
            invoice_lines = [(5,)]
            invoice_lines += [(4, p.id) for p in item.invoice_line_ids]
            invoice_lines += [(4, new_line.id)]
            item.invoice_line_ids = invoice_lines
