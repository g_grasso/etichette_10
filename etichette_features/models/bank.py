# -*- coding: utf-8 -*-

from openerp import models, fields


class Bank(models.Model):
    _inherit = 'res.partner.bank'
    _description = ""

    description = fields.Text('Description')
    partner_id = fields.Many2one(
        'res.partner',
        required=True)
