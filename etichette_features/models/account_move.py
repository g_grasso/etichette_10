# -*- coding: utf-8 -*-

from openerp import models, fields


class AccountMove(models.Model):
    _inherit = 'account.move'
    _description = "Account move"

    debt_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Outs. debt partner')

    debt_invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice outs. debt',
        help="Invoice with outstdanding debt this accont move is related to")
