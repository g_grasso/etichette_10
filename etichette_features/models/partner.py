# -*- coding: utf-8 -*-

from openerp import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'
    _description = "Etichette partner"

    partner_code = fields.Char('Partner code')
    new_vat = fields.Char('New VAT')
    letter_of_intent = fields.Text('Letter of Intent')
    official_email = fields.Char('Official email')
    folder_url = fields.Char('Folder url')
    company_bank_id = fields.Many2one(
        comodel_name='res.partner.bank',
        company_dependent=True,
        # domain=_get_company_account,
        help="Company bank account to be set in sale invoices to this partner",
        string='Company account')

    riba_charges = fields.Boolean(
        'RiBa charges',
        help="If selected, riba bank expenses shall be paid by the client")
    riba_service = fields.Many2one('product.product', 'Riba service')
