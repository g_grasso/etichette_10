# -*- coding: utf-8 -*-

from openerp import models, fields


class ProjectTask(models.Model):
    _inherit = 'project.task'

    etichette_sale_order_id = fields.Many2one(
        comodel_name='sale.order',
        string='Sale Order',
        )

    plain_description = fields.Text(
        string='Task description',
        )

    etichette_sale_order_line_id = fields.Many2one(
        comodel_name='sale.order.line',
        string='Sale Order Line',
        )
