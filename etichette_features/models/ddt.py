# -*- coding: utf-8 -*-

from openerp import models, fields, api


class StockPickingPackagePreparation(models.Model):
    _inherit = "stock.picking.package.preparation"

    disclaimer = fields.Text(
        'Disclaimer',
        default="SI PREGA DI CONTROLLARE LA MERCE AL RITIRO E ALLA CONSEGNA \n"
        "TRASCORSI 8 GIORNI DALLA DATA DI EMISSIONE NON SI ACCETTANO RESI O CONTESTAZIONI"
        )
