# -*- coding: utf-8 -*-
##############################################################################
#
#    "Etichette project"" startup project by herzog
#
##############################################################################

{
    'name': 'Etichette project',
    'version': '10.0.1.0',
    'description': """
                    Basic edits for the 'Etichette' project
        """,
    'author': '',
    'maintainer': '',
    'website': '',
    'depends': [
        'account',
        'account_accountant',
        'base',
        'delivery',
        'project',
        'product',
        'purchase',
        'sale',
        'stock',
        'stock_picking_package_preparation',
        'l10n_it_ddt',
        ],
    'data': [
        'security/access_rules.xml',
        'security/ir.model.access.csv',

        'views/account_move.xml',
        'views/bank.xml',
        'views/ddt.xml',
        'views/finance.xml',
        'views/invoice.xml',
        'views/menu.xml',
        'views/partner.xml',
        'views/payment_term.xml',
        'views/product.xml',
        'views/project.xml',
        'views/sale_order.xml',

        'report/ddt.xml',
        'report/invoice.xml',
        'report/sale_order.xml',
        'report/sale_order_2.xml',
        'report/purchase_order.xml',
        'report/ddt_no_head.xml',
        'report/invoice_no_head.xml',
        'report/partner_account_report.xml',
        'report/external_layout_header.xml',
        'report/external_layout_footer.xml',
     ],
    'installable': True,
    'auto_install': False,
}
