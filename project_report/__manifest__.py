# -*- coding: utf-8 -*-
##############################################################################
#
#
##############################################################################

{
    'name': "Project Reports",
    'summary': """Create reports of Projects Tasks""",
    'description': """
        Create reports of Projects Tasks
    """,
    'author': "Giacomo Grasso",
    'category': 'Project',
    'version': '10.0.1.1',
    'depends': [
        'project',
    ],
    'data': [
        'views/project_view.xml',
        'reports/project_report.xml',
        'reports/project_task_report.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
